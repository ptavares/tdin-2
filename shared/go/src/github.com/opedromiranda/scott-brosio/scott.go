package main

import (
    "github.com/opedromiranda/ambrosio"
    "github.com/opedromiranda/ambrosio-xkcd"
)

func main() {
    scott := ambrosio.NewAmbrosio("Scott") // give a name to the bot

    scott.Teach(ambrosio_xkcd.Behaviours)

    scott.Listen(3000)
}
