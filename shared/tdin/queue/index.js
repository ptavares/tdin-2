var SOCKET_PORT = 9998;
var STORE_SOCKET_PORT = 9997;

var zmq = require("zmq");
var store_socket = zmq.socket("pull");
var warehouse_socket = zmq.socket("push");
var counter = 0;


// Begin listening for connections on all IP addresses on port 9998.
warehouse_socket.bind("tcp://*:" + SOCKET_PORT, function (error) {
    if (error) {
        logToConsole("Failed to bind socket: " + error.message);
        process.exit(0);
    }
});

store_socket.on("message", function (message) {
    'use strict';
    console.log('got message from store, sending to warehouse');
    console.log(message);
    warehouse_socket.send(message);
});
store_socket.connect('tcp://127.0.0.1:' + STORE_SOCKET_PORT);
