module.exports = {
    ENTER_ROOM: 'ENTER_ROOM',
    INFORM_USER_COUNT: 'INFORM_USER_COUNT',
    OFFER: 'OFFER',
    ANSWER: 'ANSWER'
};
