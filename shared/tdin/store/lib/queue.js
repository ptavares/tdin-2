var STORE_SOCKET_PORT = 9997;

var zmq = require("zmq");
var socket = zmq.socket("push");

// Begin listening for connections on all IP addresses on port 9998.
socket.bind("tcp://*:" + STORE_SOCKET_PORT, function (error) {
    if (error) {
        logToConsole("Failed to bind socket: " + error.message);
        process.exit(0);
    }
});

module.exports = {
    send: function (message) {
        socket.send(message);
    }
}
