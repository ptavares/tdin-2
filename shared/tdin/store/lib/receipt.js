
module.exports = function (order, book, user) {
    var r = 'Receipt id: ' + order._id + '<br/>';
    r += 'Client name: ' + user.name + '<br/>';
    r += 'Book name: ' + book.name + '<br/>';
    r += 'Quantity: ' + order.amount + '<br/>';
    r += 'Price: ' + (Number(book.price) * Number(order.amount)) + '<br/>';
    r += 'Status: ' + order.status + '<br/>';
    return r;
};
