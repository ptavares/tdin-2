var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Book = mongoose.model('Book', {
    name: String,
    stock: Number,
    price: Number,
    to_arrive: Number
});

module.exports = Book;
