var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var User = mongoose.model('User', {
    username: String,
    password: String,
    name: String,
    email: String
});

module.exports = User;
