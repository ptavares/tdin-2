var express = require('express');
var router = express.Router();
var Book = require('../models/book.js');
var Order = require('../models/order.js');
var User = require('../models/user.js');
var receipt = require('../receipt.js');
var async = require('async');
var mail = require("nodemailer").createTransport();

function sendEmail(order, book) {
    User.findOne({
        _id: order.user_id
    }, function (err, user) {
        mail.sendMail({
            from: "TDIN Store ✔ <store@tdin.fe.up.pt>", // sender address
            to: user.email, // list of receivers
            subject: "Order " + order._id, // Subject line
            html: receipt(order, book, user)
        });
    });
}

router.post('/fulfill', function (req, res) {
    var bookData = req.body.book;

    async.parallel({
        book: function (callback) {
            Book.findOne({
                _id: bookData._id
            }, function (err, book) {
                callback(err, book);
            });
        },
        orders: function (callback) {
            Order.find({
                book_id: bookData._id
            }, function (err, order) {
                callback(err, order);
            });
        }
    }, function (e, r) {
        var book = r.book;
        var orders = r.orders;

        book.to_arrive = (book.to_arrive || 0) + bookData.missing;
        book.save();

        orders.forEach(function (order) {
            if (order.status.search("dispatched at") === -1) {
                order.status = "dispatch should occur at " + new Date(new Date().getTime() + 2 * 24 * 60 * 60 * 1000);
            }

            sendEmail(order, book);

            order.save();
        });

    });
    /*
    Book.findOne({
        _id: bookData._id
    }, function (err, book) {
        book.to_arrive = (book.to_arrive || 0) + bookData.missing;
        book.save();
    });

    Order.find({
        book_id: bookData._id
    }, function (err, orders) {
        if (err || !orders) {
            return;
        }

        orders.forEach(function (order) {
            if (order.status.search("dispatched at") === -1) {
                order.status = "dispatch should occur at " + new Date(new Date().getTime() + 2 * 24 * 60 * 60 * 1000);
            }

            sendEmail(order, book);

            order.save();
        });
    });*/

    console.log('yaaay');
    res.json({});
});

router.post('/receive', function (req, res) {
    var bookData = req.body.book;

    async.parallel({
        book: function (callback) {
            Book.findOne({
                _id: bookData._id
            }, function (err, book) {
                callback(err, book);
            });
        },
        orders: function (callback) {
            Order.find({
                book_id: bookData._id
            }, function (err, order) {
                callback(err, order);
            });
        }
    }, function (e, r) {
        var book = r.book;
        var orders = r.orders;

        book.stock = book.to_arrive;
        book.to_arrive = 0;
        book.save();
        res.json({});

        orders.forEach(function (order) {
            if (order.status.search("dispatched at") === -1) {
                order.status = "dispatched at " + new Date();
                sendEmail(order, book);
                order.save();
            }
        });
    });

    /*
    Book.findOne({
        _id: bookData._id
    }, function (err, book) {
        book.stock = book.to_arrive;
        book.to_arrive = 0;
        book.save();

        orders.forEach(function (order) {
            if (order.status.search("dispatched at") === -1) {
                order.status = "dispatched at " + new Date();
            }

            sendEmail(order, book);

            order.save();
        });

        res.json({});
    });
    */
    /*
    Order.find({
        book_id: bookData._id
    }, function (err, orders) {
        if (err || !orders) {
            return;
        }

        orders.forEach(function (order) {
            if (order.status.search("dispatched at") === -1) {
                order.status = "dispatched at " + new Date();
            }

            sendEmail(order, book);

            order.save();
        });
    });*/
});

router.get('/receipt', function (req, res) {
    var orderId = req.query.order_id;
    Order.findOne({
        _id: orderId
    }, function (err, order) {
        if (err || !order) {
            res.status(404);
            res.send('Order does not exist');
            return;
        }

        async.parallel({
            book: function (callback) {
                Book.findOne({
                    _id: order.book_id
                }, function (err, book) {
                    callback(err, book);
                });
            },
            user: function (callback) {
                User.findOne({
                    _id: order.user_id
                }, function (err, user) {
                    callback(err, user);
                });
            }
        }, function (e, r) {
            var book = r.book;
            var user = r.user;

            res.send(receipt(order, book, user));
        });
    });
});

module.exports = router;
