'use strict';

var angular = require('angular');
require('angular-ui-router');
require('angular-cookies');
require('./controllers');
require('./services');
require('./factories');
require('./directives');

angular.module('tdin-store', [
    'ngCookies',
    'ui.router',
    'tdin-store.controllers',
    'tdin-store.services',
    'tdin-store.factories',
    'tdin-store.directives'
])
    .config(['$stateProvider', '$urlRouterProvider',
        function ($stateProvider,   $urlRouterProvider) {
            $urlRouterProvider.otherwise('/');
            $stateProvider
                .state('home', {
                    url: '/',
                    templateProvider: function ($templateCache) {
                        return $templateCache.get('home.html');
                    }
                })
                .state('orders', {
                    url: '/orders',
                    templateProvider: function ($templateCache) {
                        return $templateCache.get('orders.html');
                    }
                })
                .state('admin', {
                    url: '/admin',
                    templateProvider: function ($templateCache) {
                        return $templateCache.get('admin.html');
                    }
                })
                .state('login', {
                    url: '/login',
                    templateProvider: function ($templateCache) {
                        return $templateCache.get('login.html');
                    }
                });
        }])
    .run(['$rootScope', '$state', 'session', function ($rootScope, $state, session) {
        $rootScope.$on('$stateChangeStart', function (event, toState) {

            if (toState.name !== 'login' && !session.isAuthenticated()) {
                event.preventDefault();
                $state.go('login');
            }
        });
    }]);
