'use strict';

var angular = require('angular');
var HomeController = require('./home-controller');
var AdminController = require('./admin-controller');
var LoginController = require('./login-controller');
var OrdersController = require('./orders-controller');

module.exports = angular.module('tdin-store.controllers', [])
    .controller('HomeController', HomeController)
    .controller('AdminController', AdminController)
    .controller('LoginController', LoginController)
    .controller('OrdersController', OrdersController);
