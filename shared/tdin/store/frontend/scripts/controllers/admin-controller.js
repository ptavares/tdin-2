function HomeController($scope, $state, books, orders, users, session) {
    'use strict';

    $scope.data = {
        books: [],
        users: [],
        user: session.getUser(),
        order: {
            quantity: 1,
            book: undefined,
            user: undefined
        }
    };

    books.get().success(function (books) {
        $scope.data.books = books;
    });

    users.get().success(function (users) {
        $scope.data.users = users;
    });

    $scope.selectBook = function (book) {
        $scope.data.order.book = book;
        $scope.data.order.amount = 1;
    };

    $scope.logout = function () {
        session.destroy();
        $state.go('login');
    };

    $scope.order = function () {
        var user = $scope.data.user;
        var book = $scope.data.order.book;
        var amount = $scope.data.order.quantity;
        orders.create(user, book, amount)
            .success(function (res) {
                $scope.data.order = {
                    quantity: 1,
                    book: undefined,
                    user: undefined
                };
            });
    };

    $scope.viewOrders = function () {
        $state.go('orders');
    };

    $scope.confirmDelivery = function (book) {
        console.log(book);
        books.confirmDelivery(book);
        console.log('cd');
    };

    $scope.orderValue = function () {
        var value = 0;

        if ($scope.data.order.book) {
            value = Number($scope.data.order.book.price) * Number($scope.data.order.quantity);
        }

        return value;
    };
}

HomeController.$injector = ['$scope', '$state', 'books', 'orders', 'users', 'session'];

module.exports = HomeController;
