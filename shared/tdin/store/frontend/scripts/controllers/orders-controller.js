function OrdersController($scope, $state, orders, session) {
    'use strict';

    $scope.data = {
        orders: [],
        user: session.getUser()
    };

    orders.get($scope.data.user).success(function (orders) {
        $scope.data.orders = orders;
    });

    $scope.logout = function () {
        session.destroy();
        $state.go('login');
    };

    $scope.viewProducts = function () {
        $state.go('home');
    };
}

OrdersController.$injector = ['$scope', '$state', 'orders', 'session'];

module.exports = OrdersController;
