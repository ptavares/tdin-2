function HomeController($scope, $state, $window, books, orders, session) {
    'use strict';

    $scope.data = {
        books: [],
        user: session.getUser(),
        order: {
            quantity: 0,
            book: undefined
        }
    };

    books.get().success(function (books) {
        $scope.data.books = books;
    });

    $scope.selectBook = function (book) {
        $scope.data.order.book = book;
        $scope.data.order.amount = 1;
    };

    $scope.logout = function () {
        session.destroy();
        $state.go('login');
    };

    $scope.order = function () {
        var user = $scope.data.user;
        var book = $scope.data.order.book;
        var amount = Number($scope.data.order.amount);
        orders.create(user, book, amount)
            .success(function (res) {

                $window.open('http://192.168.33.10:3000/receipt?order_id=' + res._id, '_blank');
                books.get().success(function (books) {
                    $scope.data.books = books;
                });
            });
    };

    $scope.viewOrders = function () {
        $state.go('orders');
    };
}

HomeController.$injector = ['$scope', '$state', '$window', 'books', 'orders', 'session'];

module.exports = HomeController;
