
function LoginController($scope, $state, session) {
    'use strict';

    $scope.data = {
        username: '',
        password: ''
    };

    $scope.login = function () {
        var username = $scope.data.username;
        var password = $scope.data.password;
        session.login(username, password)
            .success(function (res) {
                $state.go('home');
            });
    };
}

LoginController.$injector = ['$scope', '$state', 'session'];

module.exports = LoginController;
