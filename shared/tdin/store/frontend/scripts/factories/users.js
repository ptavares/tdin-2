'use strict';
function users($http) {

    return {
        get: function () {
            return $http({
                method: 'GET',
                url: '/api/v1/Users'
            });
        }
    };
}

users.$injector = ['$http'];

module.exports = users;
