'use strict';

var angular = require('angular');
var books = require('./books');
var order = require('./orders');
var session = require('./session');
var users = require('./users');

module.exports = angular.module('tdin-store.factories', [])
    .factory('books', books)
    .factory('orders', order)
    .factory('users', users)
    .factory('session', session);
