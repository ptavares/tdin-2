'use strict';
function books($http) {

    return {
        get: function () {
            return $http({
                method: 'GET',
                url: '/api/v1/Books'
            });
        },
        confirmDelivery: function (book) {
            return $http({
                method: 'POST',
                url: '/receive',
                data: {
                    book: book
                }
            });
        }
    };
}

books.$injector = ['$http'];

module.exports = books;
