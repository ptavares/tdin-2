
function orders($http) {
    'use strict';

    return {
        get: function (user) {
            var params = user ? {
                query: {
                    user_id: user.id
                }
            } : {};

            return $http({
                method: 'GET',
                url: '/api/v1/Orders',
                params: params
            });
        },
        create: function (user, book, amount) {
            var order = {
                user_id: user._id,
                book_id: book._id,
                amount: amount
            };

            return $http({
                headers: {
                    'Content-Type': 'application/json'
                },
                url: '/api/v1/Orders',
                method: 'POST',
                data: order
            });
        }
    };
}

orders.$injector = ['$http'];

module.exports = orders;
