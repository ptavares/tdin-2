var USER_COOKIE = 'USER';

function session($http, $cookies) {
    'use strict';
    var authenticated = false;
    var user = $cookies.getObject(USER_COOKIE);

    if (user) {
        authenticated = true;
    }

    return {
        login: function (username, password) {
            return $http({
                url: '/api/v1/Users',
                method: 'GET',
                params: {
                    query: {
                        username: username,
                        password: password
                    }
                }
            }).success(function (res) {
                user = res[0];
                authenticated = true;
                $cookies.putObject(USER_COOKIE, user);
                return res;
            });
        },
        destroy: function () {
            $cookies.remove(USER_COOKIE);
        },
        isAuthenticated: function () {
            return authenticated;
        },
        getUser: function () {
            return user;
        }
    };
}

session.$injector = ['$http', '$cookies'];

module.exports = session;
