var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var restify = require('express-restify-mongoose');
var mongoose = require('mongoose');
var async = require('async');

var routes = require('./lib/routes/index');
var users = require('./lib/routes/users');

var book = require('./lib/models/book.js');
var order = require('./lib/models/order.js');
var user = require('./lib/models/user.js');

var queue = require('./lib/queue.js');

var mail = require("nodemailer").createTransport();
var receipt = require('./lib/receipt.js');

var app = express();

mongoose.connect('mongodb://192.168.33.10/tdin-store');
//mongoose.connect('mongodb://127.0.0.1/tdin-store');
//app.configure(function () {
    //'use strict';

//});

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
restify.serve(app, book);
restify.serve(app, order, {
    postCreate: function (res, result, done) {
        'use strict';
        var user_id = result.user_id;
        var book_id = result.book_id;
        var order_id = result._id;
        var amount = result.amount;

        async.parallel({
            book: function (callback) {
                book.findOne({
                    _id: book_id
                }, function (err, book) {
                    callback(err, book);
                });
            },
            order: function (callback) {
                order.findOne({
                    _id: order_id
                }, function (err, order) {
                    callback(err, order);
                });
            },
            user: function (callback) {
                user.findOne({
                    _id: user_id
                }, function (err, user) {
                    callback(err, user);
                });
            }
        }, function (e, r) {
            var book = r.book;
            var order = r.order;
            var user = r.user;
            if (book.stock >= amount) {

                if (book.stock === Number(amount)) {
                    queue.send(JSON.stringify({
                        order: order,
                        book: book
                    }));
                }

                book.stock -= amount;
                book.save();

                order.status = "dispatched at " + new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
                order.save();
            } else {
                book.stock = 0;
                order.status = "waiting expedition";
                order.save();

                queue.send(JSON.stringify({
                    order: order,
                    book: book
                }));
            }

            mail.sendMail({
                from: "TDIN Store ✔ <store@tdin.fe.up.pt>", // sender address
                to: user.email, // list of receivers
                subject: "Order " + order._id, // Subject line
                html: receipt(order, book, user)
            });

            done();
        });
    }
});
restify.serve(app, require('./lib/models/user.js'));

app.use('/', routes);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    'use strict';
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        'use strict';
        res.status(err.status || 500);
        res.send({
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    'use strict';
    res.status(err.status || 500);
    res.send({
        message: err.message,
        error: err
    });
});

if (process.env.NODE_ENV === 'dev') {
    var debug = require('debug')('tdin-store');

    app.set('port', process.env.PORT || 3000);

    var server = app.listen(app.get('port'), function () {
        'use strict';
        debug('Express server listening on port ' + server.address().port);
    });
} else {
    module.exports = app;
}
