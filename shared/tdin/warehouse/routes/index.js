var express = require('express');
var request = require('request');
var router = express.Router();
var Book = require('../lib/models/book.js');

router.post('/fulfill', function (req, res) {
    var book = req.body.book;

    Book.findOne({
        _id: book._id
    }, function (err, book) {
        if (!err && book) {
            book.remove();
        }
    });

    request({
        url: 'http://192.168.33.10:3000/fulfill',
        method: 'POST',
        json: {
            book: book
        }
    }, function (err, res) {
        console.log(res);
    });
    res.json({});
});

module.exports = router;
