var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Order = mongoose.model('Order', {
    user_id: Schema.ObjectId,
    book_id: Schema.ObjectId,
    amount: Number,
    status: String
});

module.exports = Order;
