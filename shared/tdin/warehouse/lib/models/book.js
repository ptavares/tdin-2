var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Book = mongoose.model('Book', {
    name: String,
    stock: Number,
    price: Number,
    missing: Number
});

module.exports = Book;
