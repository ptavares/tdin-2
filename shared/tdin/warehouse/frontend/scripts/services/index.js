'use strict';

var angular = require('angular');
var books = require('./books.js');

module.exports = angular.module('tdin-store.services', [])
    .service('books', books);
