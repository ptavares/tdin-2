'use strict';
function books($http) {

    return {
        get: function () {
            return $http({
                method: 'GET',
                url: '/api/v1/Books'
            });
        }
    };
}

books.$injector = ['$http'];

module.exports = books;
