'use strict';

var angular = require('angular');
var HomeController = require('./home-controller');

module.exports = angular.module('tdin-store.controllers', [])
    .controller('HomeController', HomeController);
