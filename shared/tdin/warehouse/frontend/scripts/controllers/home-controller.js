/**
 * @file Home Controller
 * @description Contains the implementation of the Home Controller responsible
 * for allowing the users to create a new room
 */

module.exports = function HomeController($scope, $state, $http, books) {
    'use strict';

    $scope.data = {
        books: []
    };

    console.log($state);

    books.get().success(function (books) {
        $scope.data.books = books;
    });

    $scope.fulfill = function (book) {
        $http({
            url: '/fulfill',
            method: 'POST',
            data: {
                book: book
            }
        }).success(function (res) {
            var index = $scope.data.books.indexOf(book);
            $scope.data.books.splice(index, 1);
        });
    };
};
