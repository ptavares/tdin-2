'use strict';

var angular = require('angular');
require('angular-ui-router');
require('./controllers');
require('./services');
require('./factories');
require('./directives');

angular.module('tdin-store', [
    'ui.router',
    'tdin-store.controllers',
    'tdin-store.services',
    'tdin-store.factories',
    'tdin-store.directives'
])
    .config(['$stateProvider', '$urlRouterProvider',
        function ($stateProvider,   $urlRouterProvider) {
            $urlRouterProvider.otherwise('/');
            $stateProvider
                .state('home', {
                    url: '/',
                    templateProvider: function ($templateCache) {
                        return $templateCache.get('home.html');
                    }
                });
        }]);
