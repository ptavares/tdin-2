var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var restify = require('express-restify-mongoose');
var mongoose = require('mongoose');
var async = require('async');
var extend = require('extend');

var routes = require('./routes/index');
var users = require('./routes/users');

var order = require('./lib/models/order.js');
var Book = require('./lib/models/book.js');

var app = express();

mongoose.connect('mongodb://192.168.33.10/tdin-warehouse');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
restify.serve(app, Book);

app.use('/', routes);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    'use strict';
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        'use strict';
        res.status(err.status || 500);
        res.send({
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    'use strict';
    res.status(err.status || 500);
    res.send({
        message: err.message,
        error: err
    });
});

var zmq = require("zmq");
var socket = zmq.socket("pull");

// Just a helper function for logging to the console with a timestamp.
function logToConsole(message) {
    'use strict';
    console.log("[" + new Date().toLocaleTimeString() + "] " + message);
}

// Add a callback for the event that is invoked when we receive a message.
socket.on("message", function (message) {
    'use strict';
    // Convert the message into a string and log to the console.
    var data = JSON.parse(message.toString("utf8"));
    var bookData = data.book;
    var orderData = data.order;
    console.log(data);
    Book.findOne({
        name: bookData.name
    }, function (err, book) {
        if (err || !book) {
            book = new Book();
            extend(book, bookData);
        }
        book.missing = (book.missing || 0) + orderData.amount * 10;
        console.log('saving: ');
        console.log(book);
        book.save();
    });
    //logToConsole("Received message: " + message.toString("utf8"));
});

// Connect to the server instance.
socket.connect('tcp://127.0.0.1:9998');

if (process.env.NODE_ENV === 'dev') {
    var debug = require('debug')('tdin-store');

    app.set('port', process.env.PORT || 3001);

    var server = app.listen(app.get('port'), function () {
        'use strict';
        debug('Express server listening on port ' + server.address().port);
    });
} else {
    module.exports = app;
}
