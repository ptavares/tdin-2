echo "Installing NVM"
wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.25.2/install.sh | bash
[[ -s $HOME/.nvm/nvm.sh ]] && . $HOME/.nvm/nvm.sh  # This loads NVM
echo "Installing node stable"
nvm install stable
nvm use stable
nvm alias default stable

npm i gulp -g
#echo "Installing Oh-My-Zsh"
#wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O - | sh
#sed -i 's/ZSH_THEME="robbyrussell"/ZSH_THEME="kphoen"/g' ~/.zshrc

echo "export LC_ALL=\"en_US.UTF-8\"" >> ~/.bashrc
echo "export LANG=\"en_US.UTF-8\"" >> ~/.bashrc

wget http://download.zeromq.org/zeromq-3.2.5.tar.gz
tar -xzvf zeromq-3.2.5.tar.gz
cd zeromq-3.2.5
./configure
make
sudo make install
sudo ldconfig
cd ..
rm -rf zeromq-3.2.5*
