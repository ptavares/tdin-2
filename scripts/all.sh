#!/bin/sh

echo "Installing required packages"
apt-get install build-essential openssl libssl-dev curl libtool pkg-config autoconf automake g++ -y

echo "Installing Git"
apt-get install git -y

echo "Installing MongoDB"
apt-get install mongodb -y
mkdir /data
mkdir /data/db
sed -i '11s/.*/bind_ip = 0.0.0.0/' /etc/mongodb.conf

echo "Installing httpie"
apt-get install httpie -y

echo "Installing sass"
gem install sass


echo "export LC_ALL=\"en_US.UTF-8\"" >> /etc/profile
echo "export LANG=\"en_US.UTF-8\"" >> /etc/profile
#locale-gen UTF-8
#echo "Installing Zsh"
#sudo apt-get install zsh
#sudo chsh -s /bin/zsh vagrant
